#!/usr/bin/env bash

SCRIPTDIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

cd "$SCRIPTDIR"/ikiwiki &&
HOME="$SCRIPTDIR/prefix" PERL5LIB="$(pwd):$(pwd)/cpan" PERL_MM_USE_DEFAULT=1 perl -MCPAN -MCPAN::MyConfig -e 'CPAN::Shell->notest("install", "Bundle::IkiWiki")' &&
#PERL5LIB="$(pwd):$(pwd)/cpan" PERL_MM_USE_DEFAULT=1 perl -MCPAN -MCPAN::MyConfig -e 'CPAN::Shell->notest("install", "Bundle::IkiWiki::Extras")' &&
#perl Makefile.PL PREFIX="$(pwd)/.." INSTALL_BASE="$(pwd)/.." &&
PERL5LIB="$SCRIPTDIR/prefix/lib/perl5" perl Makefile.PL PREFIX="$SCRIPTDIR/prefix" INSTALL_BASE= &&
make &&
make install PREFIX="$SCRIPTDIR/prefix" &&

echo To run ikiwiki: &&
echo "$SCRIPTDIR/prefix/bin/ikiwiki"
