#!/usr/bin/env sh

SCRIPTDIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

ssh -F "$SCRIPTDIR"/auth/ssh-config "$@"

