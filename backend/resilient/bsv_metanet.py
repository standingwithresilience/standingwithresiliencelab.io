from resilient import html_path, auth_path, dep_path, git, ikiwiki
import os
import subprocess
import time

# TODO: need to connect to a local node, not a web service

class bsvup():
  binary = "bsvup"
  found = (os.system("%s --version" % binary) == 0)
  # TODO: check dep_path for alternate location for binary
  if not found:
    binary = "%s/prefix/bin/bsvup" % dep_path
    found = (os.system("%s --version" % binary) == 0)
  if not found:
    print("bsvup not found: attempting to install")
    os.system("npm install --prefix %s/prefix -g %s/bsvup" % (dep_path, dep_path))
    found = (os.system("%s --version" % binary) == 0)
  if not found:
    print("WARNING: bsvup not found.  bsv metanet won't be updated")

  def __init__(self, keyfile):
    self.keyfile = os.path.join(auth_path, keyfile)
    with open(self.keyfile) as f: self.privkey = f.read()
    self.configdir = self.keyfile + '.bsv'
    if bsvup.found:
      if not os.path.isfile(os.path.join(self.configdir, 'key')):
        if os.path.isdir(self.configdir):
          os.remove(os.path.join(self.configdir, '.bsv'))
        else:
          os.mkdir(self.configdir)
        os.symlink(".", os.path.join(self.configdir, ".bsv"))
        proc = subprocess.Popen([bsvup.binary, '-p', '', '-r', '250', 'init'], stdin=subprocess.PIPE, cwd=self.configdir)
        result = proc.communicate(self.privkey + "\n");
        print(result[0])
        print(result[1])
        if not os.path.isfile(os.path.join(self.configdir, 'key')):
            raise "key not generated"
        open(os.path.join(self.configdir, "info", ".keepme"), "a").close()
        open(os.path.join(self.configdir, "objects", ".keepme"), "a").close()
        open(os.path.join(self.configdir, "tx", ".keepme"), "a").close()
      else: # resume tx if needed
        proc = subprocess.Popen([bsvup.binary], stdin=subprocess.PIPE, cwd=self.configdir)
        proc.communicate("Y\n")

  def push(self):
    if not ikiwiki.synced or not bsvup.found:
      return False

    # add files to bsv metanet
    orig_dir = os.path.abspath(".")
    proc = subprocess.Popen([bsvup.binary, '-p', '', '-r', '250', '-f', html_path, 'upload'], stdin=subprocess.PIPE, cwd=self.configdir)
    proc.stdin.write("\n")
    time.sleep(1)
    proc.stdin.write("Y\n")
    return proc.wait() == 0
