from resilient import auth_path, dep_path, git, ikiwiki, url200
import os

running = url200('http://127.0.0.1:8888/')
if not running:
  print("WARNING: freenet not running, changes won't be synced with freenet")

class gitocalypse(git):
  found = (os.system("git-remote-freenet") == 256)
  if not found:
    print("WARNING: gitocalypse not found, changes won't be synced with freenet")

class freesitemgr():
  binary = "freesitemgr"
  found = (os.system("%s --version" % binary) == 0)
  if not found:
    binary = "%s/pyFreenet/freesitemgr" % dep_path
    found = (os.system("%s --version" % binary) == 0)
  if not found:
    print("WARNING: freesitemgr/pyFreenet not found. freesite won't be updated")

  def __init__(self):
    pass

  def push(self):
    if not ikiwiki.synced or not freesitemgr.found or not running:
      return False
    config_path = os.path.join(auth_path, 'freesitemgr')
    status = os.system("%s -c '%s' -v -r 1 update" % (freesitemgr.binary, config_path))
    return status == 0
