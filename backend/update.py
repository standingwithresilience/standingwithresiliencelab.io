#!/usr/bin/env python2

import resilient
import resilient.freenet
import resilient.bsv_metanet


####################### Instantiate hosts

hosts = []

zeronet_urls = ['https://0net.io']
if resilient.zeronet.found:
  hosts.append(resilient.zeronet('1GtQ8bkFkhYtKerSdXHKe1z4j9VTmFR5d4',
                                 '5JzZACCELsT4bx66qmV9JDaWhEQ6Sx6j7LzeucsxJGA9H7nuoRr'))
  zeronet_urls.append('http://127.0.0.1:43110')

bsv_urls = ['https://bico.media']
if resilient.bsv_metanet.bsvup.found:
  bsvup_host = resilient.bsv_metanet.bsvup('bsv-public')
  hosts.append(bsvup_host)

ipfs_urls = ['https://ipfs.io']
if resilient.ipfs.found:
  ipfs_host = resilient.ipfs('ipfs-public')
  ipfs_urls.append(ipfs_host.gateway)
  hosts.append(ipfs_host)

if resilient.freenet.freesitemgr.found and resilient.freenet.running:
  hosts.append(resilient.freenet.freesitemgr()) # keys for freesitemgr are in auth/freesitemgr


##################### Instantiate remotes

git_repos = [
  resilient.git('ssh://b-standingwithresilience@standingwithresilience.branchable.com/'),
  resilient.git('fairlystable-guest:/srv/git/standingwithresilience'),
  resilient.git('https://gitlab.com/standingwithresilience/standingwithresilience.gitlab.io.git', None)
]

for host in zeronet_urls:
  git_repos.extend([
    #### public zeronet ####
    resilient.git('%s/1GtQ8bkFkhYtKerSdXHKe1z4j9VTmFR5d4/git' % host, None),
    #### private zeronet ####
    resilient.git('%s/1L363bqJnCG63SnV83kfV7izZXbmentctD/git' % host, None)
  ])

for host in bsv_urls:
  git_repos.extend([
    #### public bsv ####
    resilient.git('%s/18xAQuoytYNuXrZwoLvLHmHwS3dNqw5BXF/git' % host, None)
  ])

#for host in ipfs_urls:
#  git_repos.extend([
#    #### public ipfs ####
#    resilient.git('%s/ipns/QmPw1ZqrdEma823zTrnH9cVFF4j3YSeedrRjqPGj9Q8Utx/git' % host, None),
#    #### private ipfs ####
#    resilient.git('%s/ipns/QmYMViYhjtwH2BkuHYRGJvPDWcXdNf2YcRFTTCzzqQGVn1/git' % host, None)
#  ])

if resilient.freenet.gitocalypse.found and resilient.freenet.running:
  git_repos.extend([
    #### public freenet ####
    resilient.freenet.gitocalypse('freenet://USK@hCLgfaINNSNAl4do-PapEincQP5Lxa72d8mFrzHqzqU,jjuBPGO~oCByVoZ5f4Bny0Gp-l3kuDOtf3m-QBT4ekA,AQACAAE/Standing%20With%20Resilience%20Public.git.R1/0',
                                  'freenet://USK@AKL0griFs7T25yw6-JR3Wk7vocvNBelOLg3RwtIL6aTp,jjuBPGO~oCByVoZ5f4Bny0Gp-l3kuDOtf3m-QBT4ekA,AQECAAE/Standing%20With%20Resilience%20Public.git.R1/0'),
    #### private freenet ####
    resilient.freenet.gitocalypse('freenet://USK@PSY0YngeDtGbj60hjEoilQiY9oKzVrptRC0rG4BEVPg,e6ZEjT4KvbsltPBJOeKNpIdty2oGzbLCdk4fsDA5Vdk,AQACAAE/Standing%20With%20Resilience.git.R1/0')
  ])


####################### Download from all

print(":: Pulling from git repos ...")

for repo in git_repos:
  repo.pull()

########################### Upload to all

print(":: Pushing to git repos ...")

for repo in git_repos:
  repo.push()


############################## Build wiki

if not resilient.ikiwiki.found:
  print (":: Not building wiki; missing ikiwiki")
  exit(0)

print (":: Building wiki ...")

if not resilient.ikiwiki().push():
  print (":: Exiting due to failure")
  exit(-1)

########################### Upload to all

print (":: Publishing site ...")

for host in hosts:
  host.push()

#########################################

print ("Done.")
